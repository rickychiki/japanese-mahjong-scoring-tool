function userGestures() {
    const modal = document.getElementById('user-gestures');
    modal.addEventListener('click', async () => {
        await game.getSound();
        modal.remove();
    }, { once: true });
};

//select dealer
function addDealerEventListerner(restart) {
    const e = async (event) => {
        event.stopPropagation()
        game.playAudio(game.audio.no_sound, true);
        const qsStartBox = event.currentTarget.parentElement.className
        //set dealer
        game.dealer = qsStartBox
        game.startingPlayer = qsStartBox
        //remove all box event
        boxes.forEach(el => el.removeEventListener('click', e));
        if (!restart) {
            game.addEventListener()
        } else {
            document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"))
        }
        game.start()
    }
    const boxes = document.querySelectorAll('.box:not(.center)');
    boxes.forEach(el => el.addEventListener('click', e))
}


class Player {
    constructor(position_variable, initial_point) {
        this.point = initial_point
        this.previewPoint = 0
        this.changed = 0
        this.isDealer = 0
        this.isRiichi = 0
        this.isCalling = 0
        this.isFuro = undefined
        this.discarder = 0
        this.tiePriority = 0
        this.position = position_variable
        this.name = ""
    }
    add(point_variable) {
        this.point = this.point + point_variable;
        return this.point;
    }
    sub(point_variable) {
        this.point = this.point - point_variable;
        return this.point;
    }
    result() {
        return this.point;
    }
    set(point_variable) {
        this.point = point_variable;
        return this.point;
    }
}

class Deposit extends Player {
    constructor(position_variable, initial_point) {
        super(position_variable, initial_point)
    }
}

class Game {
    constructor(initial_point) {
        this.startingPlayer = ""
        this.dealer = ""
        this.top = new Player("top", initial_point)
        this.bottom = new Player("bottom", initial_point)
        this.left = new Player("left", initial_point)
        this.right = new Player("right", initial_point)
        this.deposit = new Deposit("center", 0)
        this.hand = 1 //e1,e2,e3,e4,e4-1,s1 =1,2,3,4,4,5
        this.repeater = 0
        this.draw = 0
        this.isEnd = 0
        this.nonCallPoint = 3000
        this.round = "東"
        this.initialPoint = initial_point
        this.originPoint = initial_point + 5000
        this.history = [] //this.history.length - 1 = total hand(s) played
        this.isConfirm = 0
        this.isHandlingMultiRon = false
        this.players = [this.top, this.left, this.bottom, this.right]
        this.players_add = []
        this.players_sub = []
        this.changedPointHistory = []
        this.multiRonData = [];
        this.uuid = ""
        this.audioList = ["no_sound", "welcome", "otsukare", "ganbattene", "issyoniganbarimasyou", "omedeto", "sugoi", "yattane", "yokattane", "start", "end", "ton", "nan", "sha", "pei", "ton2", "nan2", "sha2", "pei2", "ikkyoku", "nikyoku", "sankyoku", "yonkyoku", "ipponba", "nihonba", "sanbonba", "yonhonba", "gohonba", "ropponba", "nanahonba", "happonba", "kyuhonba", "jupponba", "juipponba", "junihonba", "jusanbonba", "juyonhonba", "jugohonba", "juropponba", "reach", "tsumo", "ron", "agari", "mangan", "haneman", "baiman", "sanbaiman", "yakuman"]
        this.han = [0, 1, 2, 3, 4, 5, "6-7", "8-10", "11-12", 13]
        this.fu = [0, 30, 30, 30, 40, 50, 60, 70, 80, 90, 100, 110]
        this.basicPoint = [2000, 3000, 4000, 6000, 8000, 16000, 24000, 32000]
        this.audio = {};
        this.AudioContext = "";
    }

    playAudio(buffer, loop = false) {
        if (buffer instanceof AudioBuffer === false) {
            console.log(buffer + ' buffer not found');
        }
        return new Promise((resolve) => {
            const source = this.AudioContext.createBufferSource();
            source.buffer = buffer;
            source.connect(this.AudioContext.destination);
            source.loop = loop;
            source.start();
            source.onended = () => {
                resolve('ended');
            }
        })
    };
    async getBuffer(fileName) {
        const res = await fetch(`sound/${fileName}.mp3`, {
            method: "GET",
            responseType: "arraybuffer"
        });
        let result = await res.arrayBuffer();
        return new Promise((resolve, reject) => {
            res.status !== 200 ? reject(fileName + ' not found') : this.AudioContext.decodeAudioData(result, buffer => buffer ? resolve(buffer) : reject('decoding error'), e => { reject('decoding error'); console.log(e) });
        })
    };
    async getSound() {
        this.AudioContext = new (window.AudioContext || window.webkitAudioContext)();
        let list = this.audioList.map(async (audio) => {
            try {
                this.audio[audio] = await game.getBuffer("female1_" + audio);
            } catch (e) {
                //file not found
                console.log(e)
            }
        })
        return Promise.all(list)
    }

    async send() {
        let data = this.history
        const res = await fetch(`/server/${this.uuid}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        });
        return await res.json()
    }

    async getUUID() {
        const res = await fetch('/start');
        return await res.json()
    }

    record(detail) {
        this.history.push(
            {
                date: new Date().toLocaleString('en-US'),
                gameDetail: {
                    round: this.round,
                    hand: this.hand % 4 == 0 ? 4 : this.hand % 4,
                    repeater: this.repeater,
                    deposit: game.deposit.point / 1000,
                    dealer: this.dealer
                },
                handDetail: detail,
                playerDetail: [
                    { name: game.top.name, position: "top", origin: game.history[game.history.length - 1].playerDetail[0].point, changed: game.top.changed, point: game.top.point, riichi: game.top.isRiichi ? true : false, furo: typeof game.top.isFuro === 'undefined' ? undefined : game.top.isFuro ? true : false },
                    { name: game.left.name, position: "left", origin: game.history[game.history.length - 1].playerDetail[1].point, changed: game.left.changed, point: game.left.point, riichi: game.left.isRiichi ? true : false, furo: typeof game.left.isFuro === 'undefined' ? undefined : game.left.isFuro ? true : false },
                    { name: game.bottom.name, position: "bottom", origin: game.history[game.history.length - 1].playerDetail[2].point, changed: game.bottom.changed, point: game.bottom.point, riichi: game.bottom.isRiichi ? true : false, furo: typeof game.bottom.isFuro === 'undefined' ? undefined : game.bottom.isFuro ? true : false },
                    { name: game.right.name, position: "right", origin: game.history[game.history.length - 1].playerDetail[3].point, changed: game.right.changed, point: game.right.point, riichi: game.right.isRiichi ? true : false, furo: typeof game.right.isFuro === 'undefined' ? undefined : game.right.isFuro ? true : false }
                ]
            }
        )
    }
    //start
    async start() {
        let playersName = [];
        while (playersName.length !== 4) {
            let inputName = window.prompt('player name(top,left,bottom,right)?', 'player1,player2,player3,player4')
            if (inputName) playersName = inputName.split(',')
        }
        // playersName = 'player1,player2,player3,player4'.split(',')
        this.top.name = playersName[0]
        this.left.name = playersName[1]
        this.bottom.name = playersName[2]
        this.right.name = playersName[3]
        document.querySelector('.top .player-name').innerHTML = playersName[0]
        document.querySelector('.left .player-name').innerHTML = playersName[1]
        document.querySelector('.bottom .player-name').innerHTML = playersName[2]
        document.querySelector('.right .player-name').innerHTML = playersName[3]
        this.uuid = await this.getUUID()
        console.log(this.uuid)
        this.history.push(
            {
                date: new Date().toLocaleString('en-US'),
                starting_player: this.startingPlayer,
                gameDetail: {
                    round: this.round,
                    hand: this.hand % 4 == 0 ? 4 : this.hand % 4,
                    repeater: this.repeater,
                    deposit: game.deposit.point / 1000,
                    dealer: this.dealer
                },
                playerDetail: [
                    { name: game.top.name, position: "top", origin: this.initialPoint, point: this.initialPoint },
                    { name: game.left.name, position: "left", origin: this.initialPoint, point: this.initialPoint },
                    { name: game.bottom.name, position: "bottom", origin: this.initialPoint, point: this.initialPoint },
                    { name: game.right.name, position: "right", origin: this.initialPoint, point: this.initialPoint }
                ]
            }
        )
        document.querySelector(`.top .current.point`).innerHTML = game.top.result()
        document.querySelector(`.bottom .current.point`).innerHTML = game.bottom.result()
        document.querySelector(`.left .current.point`).innerHTML = game.left.result()
        document.querySelector(`.right .current.point`).innerHTML = game.right.result()
        game[game.startingPlayer].isDealer = 1
        game[game.startingPlayer].tiePriority = 3
        if (game.dealer == "top") {
            game.left.tiePriority = 2
            game.bottom.tiePriority = 1
        }
        if (game.dealer == "left") {
            game.bottom.tiePriority = 2
            game.right.tiePriority = 1
        }
        if (game.dealer == "bottom") {
            game.right.tiePriority = 2
            game.top.tiePriority = 1
        }
        if (game.dealer == "right") {
            game.top.tiePriority = 2
            game.left.tiePriority = 1
        }
        document.querySelector(`.${CSS.escape(this.dealer)} .box`).classList.add("dealer")
        await game.playAudio(game.audio.start)
        game.playGameSound();
    }
    //add event listener
    addEventListener() {
        //When setting clicked
        document.querySelector('.setting').addEventListener('click', (event) => {
            event.stopPropagation()
            if (this.isHandlingMultiRon) return;
            if (game.history.length >= 2) {
                let oldHistory = game.history.slice();
                game.history = game.history.filter(x => x.gameDetail);
                let backHistory = game.history.length - 1
                let ans = confirm(`back to ${game.history[backHistory].gameDetail.round}${game.history[backHistory].gameDetail.hand}局 ${game.history[backHistory].gameDetail.repeater}本場?`)
                if (ans == true) {
                    if (game.isEnd == 1) {
                        game.isEnd = 0
                    }
                    game.round = game.history[backHistory].gameDetail.round
                    switch (game.history[backHistory].gameDetail.round) {
                        case "東":
                        default:
                            game.hand = game.history[backHistory].gameDetail.hand
                            break;
                        case "南":
                            game.hand = game.history[backHistory].gameDetail.hand + 4
                            break;
                        case "西":
                            game.hand = game.history[backHistory].gameDetail.hand + 8
                            break;
                        case "北":
                            game.hand = game.history[backHistory].gameDetail.hand + 12
                            break;
                    }
                    game.repeater = game.history[backHistory].gameDetail.repeater
                    game.deposit.point = game.initialPoint * 4 - game.history[backHistory].playerDetail[0].origin - game.history[backHistory].playerDetail[1].origin - game.history[backHistory].playerDetail[2].origin - game.history[backHistory].playerDetail[3].origin
                    game[game.dealer].isDealer = 0
                    document.querySelectorAll(".box").forEach(el => el.classList.remove("dealer"))
                    game[game.history[backHistory].gameDetail.dealer].isDealer = 1
                    document.querySelector(`.${CSS.escape(game.history[backHistory].gameDetail.dealer)} .box`).classList.add("dealer")
                    game.dealer = game.history[backHistory].gameDetail.dealer
                    console.log(game.history[backHistory].playerDetail)
                    game.top.point = game.history[backHistory].playerDetail[0].origin
                    game.left.point = game.history[backHistory].playerDetail[1].origin
                    game.bottom.point = game.history[backHistory].playerDetail[2].origin
                    game.right.point = game.history[backHistory].playerDetail[3].origin

                    //remove result listener
                    let el = document.getElementsByClassName('result')[0];
                    let elClone = el.cloneNode(true);
                    el.parentNode.replaceChild(elClone, el);
                    document.getElementsByClassName('result')[0].style.display = 'none'


                    document.querySelector(".hand").innerHTML = game.hand % 4 == 0 ? 4 : game.hand % 4
                    document.querySelector(".roundText").innerHTML = game.round
                    document.querySelector(".repeater").innerHTML = game.repeater
                    document.querySelector(".deposit").innerHTML = game.deposit.point / 1000
                    document.querySelector(`.top .current.point`).innerHTML = game.top.result()
                    document.querySelector(`.bottom .current.point`).innerHTML = game.bottom.result()
                    document.querySelector(`.left .current.point`).innerHTML = game.left.result()
                    document.querySelector(`.right .current.point`).innerHTML = game.right.result()
                    // console.log("history backed")
                    game.history.pop()
                    game.resetFuro()
                } else {
                    game.history = oldHistory;
                }
            }
        })
        // when multi clicked
        document.querySelector('.multi').addEventListener('click', (event) => {
            event.stopPropagation();
            if (this.isHandlingMultiRon) return;
            document.querySelectorAll('button.multi-ron').forEach(el => { el.innerHTML = '--'; el.classList.remove("ron"); el.classList.remove("discard") });
            document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"))
            document.querySelector(`.center .multi-ron.menu`).classList.add("activated");
        });
        //When reset clicked
        document.querySelector('.reset').addEventListener('click', (event) => {
            event.stopPropagation();
            let ans = confirm(`Reset this game?`);
            if (!ans) return
            let confirmAns = confirm(`Are you sure reset this game?`);;
            if (confirmAns == true) {
                game.newGame();
            }
        });

        document.querySelectorAll('.sub.menu').forEach(el => el.addEventListener('click', (event) => {
            const subMenu = event.currentTarget.parentElement.className;
            if (this[subMenu].isRiichi !== 1) {
                if (typeof this[subMenu].isFuro === 'undefined') {
                    this[subMenu].isFuro = 0;
                    document.querySelector(`.${CSS.escape(subMenu)} .sub.menu`).classList.add('blue');
                    document.querySelector(`.${CSS.escape(subMenu)} .box`).classList.add('blue');
                } else if (this[subMenu].isFuro === 0) {
                    this[subMenu].isFuro = 1;
                    document.querySelector(`.${CSS.escape(subMenu)} .sub.menu`).classList.remove('blue');
                    document.querySelector(`.${CSS.escape(subMenu)} .box`).classList.remove('blue');
                    document.querySelector(`.${CSS.escape(subMenu)} .sub.menu`).classList.add('red')
                    document.querySelector(`.${CSS.escape(subMenu)} .box`).classList.add('red');
                } else if (this[subMenu].isFuro === 1) {
                    this[subMenu].isFuro = undefined;
                    document.querySelector(`.${CSS.escape(subMenu)} .sub.menu`).classList.remove('red');
                    document.querySelector(`.${CSS.escape(subMenu)} .box`).classList.remove('red');
                };
            };
        }));

        let timer;
        document.querySelectorAll('.box:not(.center)').forEach(el => el.addEventListener('touchstart', (event) => {
            const qsBox = event.currentTarget.parentElement.className;
            document.querySelector(`.top .changed.point`).innerHTML = game[qsBox].result() - game.top.result();
            document.querySelector(`.left .changed.point`).innerHTML = game[qsBox].result() - game.left.result();
            document.querySelector(`.bottom .changed.point`).innerHTML = game[qsBox].result() - game.bottom.result();
            document.querySelector(`.right .changed.point`).innerHTML = game[qsBox].result() - game.right.result();
            document.querySelector(`.${CSS.escape(qsBox)}  .changed.point`).innerHTML = "-";
            timer = setTimeout(() => {
                game.showPointDifference(true);
            }, 1000);
        }))

        document.querySelectorAll('.box:not(.center)').forEach(el => el.addEventListener('touchend', (event) => {
            clearTimeout(timer);
            game.showPointDifference(false);
        }))

        //When top box clicked remove all menu include submenu 
        for (let box of document.querySelectorAll('.box')) {
            box.addEventListener('click', (event) => {
                event.stopPropagation()
                //currentTarget only box, target include box, stick, detail
                const qsBox = event.currentTarget.parentElement.className
                const boxAllMenuActivated = Array.from(document.querySelectorAll('.menu')).find(x => x.className.includes('activated'));
                const boxActivated = Array.from(document.querySelectorAll('.sub.menu')).find(x => x.className.includes('activated'));
                const centerBoxMenuActivated = document.querySelector('.center .main.menu').className.includes('activated');
                if (boxActivated || centerBoxMenuActivated) {
                    if (qsBox !== 'center' && this[qsBox].isRiichi !== 1) {
                        if (typeof this[qsBox].isFuro === 'undefined') {
                            this[qsBox].isFuro = 0;
                            document.querySelector(`.${CSS.escape(qsBox)} .box`).classList.add('blue');
                            document.querySelector(`.${CSS.escape(qsBox)} .sub.menu`).classList.add('blue');
                        } else if (this[qsBox].isFuro === 0) {
                            this[qsBox].isFuro = 1;
                            document.querySelector(`.${CSS.escape(qsBox)} .box`).classList.remove('blue');
                            document.querySelector(`.${CSS.escape(qsBox)} .sub.menu`).classList.remove('blue');
                            document.querySelector(`.${CSS.escape(qsBox)} .box`).classList.add('red')
                            document.querySelector(`.${CSS.escape(qsBox)} .sub.menu`).classList.add('red');
                        } else if (this[qsBox].isFuro === 1) {
                            this[qsBox].isFuro = undefined;
                            document.querySelector(`.${CSS.escape(qsBox)} .box`).classList.remove('red');
                            document.querySelector(`.${CSS.escape(qsBox)} .sub.menu`).classList.remove('red');
                        };
                    };
                    return;
                };
                if (boxAllMenuActivated) {
                    return;
                }
                if (this.isHandlingMultiRon) return;
                this.resetDiscarder()
                this.resetCalling()
                this.isConfirm = 0
                document.querySelectorAll('.selected').forEach(el => el.classList.remove("selected"))
                document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"))
                //When menu cancel clicked remove menu only
                document.querySelector(`.${CSS.escape(qsBox)} .main.menu .cancel`).addEventListener('click', (event) => {
                    event.stopPropagation()
                    document.querySelector(`.${CSS.escape(qsBox)} .main.menu`).classList.remove("activated")
                }, { once: true })
                document.querySelector(`.${CSS.escape(qsBox)} .main.menu`).classList.add("activated")
                this.players.map(el => {
                    if (el.isRiichi == 1) {
                        document.querySelector(`.${el.position} .riichi`).innerHTML = "取消立直"
                    } else {
                        document.querySelector(`.${el.position} .riichi`).innerHTML = "立直"
                    }
                })
                if (qsBox === 'center') {
                    document.querySelectorAll('.calling').forEach(el => el.innerHTML = '沒聽')
                    document.querySelector(`.center .main.menu`).classList.add("activated")
                    this.players.map(el => {
                        if (el.isRiichi == 1) {
                            el.isCalling = 1
                            el.isFuro = 0
                            document.querySelector(`.${el.position} .box`).classList.add("blue")
                            document.querySelector(`.calling.${el.position}`).classList.add("selected")
                            document.querySelector(`.calling.${el.position}`).innerHTML = '聽牌'
                        }
                    })
                }
            })
        }

        //When center confirm clicked, calc the draw
        document.querySelector('.center .main.menu .confirm').addEventListener('click', (event) => {
            event.stopPropagation()
            if (game.isConfirm == 1) {
                game.players_add = game.players.filter(players => players.isCalling == 1)
                game.players_sub = game.players.filter(players => players.isCalling == 0)
                if (game.players_add.length != 4 && game.players_sub.length != 4) {
                    for (let i = 0; i < game.players_add.length; i++) {
                        game.players_add[i].changed += (game.nonCallPoint / game.players_add.length)
                        game.players_add[i].add(game.players_add[i].changed)
                    }
                    for (let i = 0; i < game.players_sub.length; i++) {
                        game.players_sub[i].changed -= (game.nonCallPoint / game.players_sub.length)
                        game.players_sub[i].add(game.players_sub[i].changed)
                    }
                }
                document.querySelector(`.top .current.point`).innerHTML = game.top.result()
                document.querySelector(`.bottom .current.point`).innerHTML = game.bottom.result()
                document.querySelector(`.left .current.point`).innerHTML = game.left.result()
                document.querySelector(`.right .current.point`).innerHTML = game.right.result()
                game.changedPointColor()
                game.players.filter(players => players.isRiichi == 1).map(players => players.changed -= 1000)
                game.isConfirm = 0
                if (game.players.filter(players => players.isRiichi == 1).length === 4) {
                    game.record({ state: "流局", abortive: "四家立直" })
                } else {
                    game.record({ state: "流局" })
                }
                console.table({
                    game: game.history[game.history.length - 1].gameDetail.round + game.history[game.history.length - 1].gameDetail.hand + "局 " + game.history[game.history.length - 1].gameDetail.repeater + "本場 " + "親: " + game.history[game.history.length - 1].gameDetail.dealer,
                    ...game.history[game.history.length - 1].handDetail, date: game.history[game.history.length - 1].date
                })
                console.table([...game.history[game.history.length - 1].playerDetail, { position: "center", point: game.deposit.point }])
                game.resetChanged()
                game.resetRiichi()
                game.resetFuro()
                game.resetCalling()
                document.querySelectorAll('.selected').forEach(el => el.classList.remove("selected"))
                document.querySelector(`.center .main.menu`).classList.remove("activated")
                game.changeDealer(game.players_add.filter(players => players.isDealer == 1).map(x => x.position))
                game.checkEnd()
                if (game.isEnd !== 1) this.playGameSound();
            } else {
                return game.isConfirm = 1
            }
        })
        //When center cancel clicked, hide calling menu
        document.querySelector('.center .main.menu .cancel').addEventListener('click', (event) => {
            event.stopPropagation()
            document.querySelector(`.center .main.menu`).classList.remove("activated")
            this.isConfirm = 0
            this.resetCalling()
            this.resetFuro()
            document.querySelectorAll('.selected').forEach(el => el.classList.remove("selected"))
        })
        //When center multi-ron confirm clicked
        document.querySelector('.center .multi-ron.menu .confirm').addEventListener('click', (event) => {
            event.stopPropagation();
            let multi = [];
            let regex = /multi-ron (\w{1,}) ?(\w{1,})?/;
            document.querySelectorAll('button.multi-ron').forEach(x =>
                multi.push({ pos: x.className.replace(regex, '$1'), status: x.className.replace(regex, '$2') })
            )
            let discarder = multi.filter(x => x.status === 'discard');
            let ron = multi.filter(x => x.status === 'ron');
            if (discarder.length !== 1 || ron.length !== 2) return;
            game.playAudio(game.audio.ron);
            document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"));
            this.players.map(el => {
                if (el.isRiichi == 1) {
                    el.isFuro = 0;
                    document.querySelector(`.${el.position} .box`).classList.add("blue");
                    document.querySelector(`.${el.position} .sub.menu`).classList.add("blue");
                };
            });
            let ronArray;
            if (discarder[0].pos === 'top') ronArray = ['top', 'left', 'bottom', 'right'];
            if (discarder[0].pos === 'left') ronArray = ['left', 'bottom', 'right', 'top'];
            if (discarder[0].pos === 'bottom') ronArray = ['bottom', 'right', 'top', 'left'];
            if (discarder[0].pos === 'right') ronArray = ['top', 'left', 'bottom', 'right'];
            ron.sort((a, b) => ronArray.indexOf(a.pos) < ronArray.indexOf(b.pos) ? -1 : 1)
            game.isHandlingMultiRon = true;
            const seat = {
                top: {
                    left: 'lower',
                    bottom: 'opposite',
                    right: 'upper'
                },
                left: {
                    bottom: 'lower',
                    right: 'opposite',
                    top: 'upper',
                },
                bottom: {
                    right: 'lower',
                    top: 'opposite',
                    left: 'upper'
                },
                right: {
                    top: 'lower',
                    left: 'opposite',
                    bottom: 'upper'
                }
            }
            const openMenu = (pos) => {
                document.querySelector(`.${pos} .han`).value = 1
                document.querySelector(`.${pos} .fu`).value = 3
                document.querySelector(`.${pos} .han_value`).innerHTML = 1
                document.querySelector(`.${pos} .fu_value`).innerHTML = 30
                document.querySelector(`.${pos} .win`).innerHTML = "榮和"
                this.fu = [0, 30, 30, 30, 40, 50, 60, 70, 80, 90, 100, 110]
                const previewPoint = game.scorer().ron(game.han[document.querySelector(`.${pos} .han`).value], game.fu[document.querySelector(`.${pos} .fu`).value], game[pos].isDealer);
                this[pos].previewPoint = previewPoint;
                document.querySelector(`.${pos} .preview.point`).innerHTML = previewPoint;
                document.querySelector(`.${pos} .dealer-text`).innerHTML = game[pos].isDealer ? "親" : "子"
                document.querySelector(`.${pos} .repeater`).innerHTML = game.repeater
                document.querySelector(`.${pos} .sub.menu`).classList.add("activated")
                document.querySelector(`.${pos} .discarder`).classList.add("activated")
                document.querySelector(`.${pos} .discarder .${seat[pos][discarder[0].pos]}`).classList.add("selected")
                this[pos].discarder = document.querySelector(`.${pos} .discarder .${seat[pos][discarder[0].pos]}`).className
            }
            openMenu(ron[0].pos)
            function a() {
                setTimeout(() => {
                    openMenu(ron[1].pos)
                }, 150);
            }
            document.querySelector(`.${ron[0].pos} .sub.menu .confirm`).addEventListener('click', a, { once: true })
            document.querySelector(`.${ron[1].pos} .sub.menu .confirm`).addEventListener('click', () => {
                game.isHandlingMultiRon = false
                document.querySelectorAll('.selected').forEach(el => el.classList.remove("selected"))
            }, { once: true })
        });
        //When center cancel clicked, hide calling menu
        document.querySelector('.center .multi-ron.menu .cancel').addEventListener('click', (event) => {
            event.stopPropagation();
            document.querySelector(`.center .multi-ron.menu`).classList.remove("activated");
        });
        //When center calling clicked, add new class
        for (let calling of document.querySelectorAll('.calling')) {
            calling.addEventListener('click', (event) => {
                const qsCalling = event.target.className.replace(/(calling)|(selected)|[ ]/g, "")
                event.stopPropagation()
                if (event.target.classList.contains("selected")) {
                    event.target.classList.remove("selected")
                    event.target.innerHTML = '沒聽'
                    game[qsCalling].isCalling = 0
                } else {
                    event.target.classList.add("selected")
                    event.target.innerHTML = '聽牌'
                    game[qsCalling].isCalling = 1
                }
            })
        }
        //When center multi-ron clicked, add new class
        for (let multiron of document.querySelectorAll('button.multi-ron')) {
            multiron.addEventListener('click', (event) => {
                event.stopPropagation();
                if (event.target.classList.contains("ron")) {
                    event.target.classList.remove("ron");
                    event.target.classList.add("discard");
                    event.target.innerHTML = '放銃';
                } else if (event.target.classList.contains("discard")) {
                    event.target.classList.remove("discard");
                    event.target.innerHTML = '--';
                } else {
                    event.target.classList.add("ron")
                    event.target.innerHTML = '榮和';
                };
            });
        }
        //When top tsumo clicked, submenu activate and innerHTMl change
        for (let tsumo of document.querySelectorAll('.tsumo')) {
            tsumo.addEventListener('click', (event) => {
                event.stopPropagation()
                game.playAudio(game.audio.tsumo)
                this.players.map(el => {
                    if (el.isRiichi == 1) {
                        el.isFuro = 0;
                        document.querySelector(`.${el.position} .box`).classList.add("blue");
                        document.querySelector(`.${el.position} .sub.menu`).classList.add("blue");
                    };
                });
                const qsTsumo = event.target.parentElement.parentElement.parentElement.className
                game.fu = [0, 30, 30, 30, 40, 50, 60, 70, 80, 90, 100, 110]
                document.querySelector(`.${CSS.escape(qsTsumo)} .han`).value = 1
                document.querySelector(`.${CSS.escape(qsTsumo)} .fu`).value = 3
                document.querySelector(`.${CSS.escape(qsTsumo)} .han_value`).innerHTML = 1
                document.querySelector(`.${CSS.escape(qsTsumo)} .fu_value`).innerHTML = 30
                document.querySelector(`.${CSS.escape(qsTsumo)} .win`).innerHTML = "自摸"
                const previewPoint = game.scorer().tsumo(game.han[document.querySelector(`.${CSS.escape(qsTsumo)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsTsumo)} .fu`).value], game[qsTsumo].isDealer);
                this[qsTsumo].previewPoint = previewPoint;
                document.querySelector(`.${CSS.escape(qsTsumo)} .preview.point`).innerHTML = previewPoint;
                document.querySelector(`.${CSS.escape(qsTsumo)} .repeater`).innerHTML = game.repeater
                document.querySelector(`.${CSS.escape(qsTsumo)} .dealer-text`).innerHTML = game[qsTsumo].isDealer ? "親" : "子"
                document.querySelector(`.${CSS.escape(qsTsumo)} .sub.menu`).classList.add("activated")
                document.querySelector(`.${CSS.escape(qsTsumo)} .discarder`).classList.remove("activated")
                document.querySelector(`.${CSS.escape(qsTsumo)} .main.menu`).classList.remove("activated")
            })
        }
        //When top ron clicked, submenu activate and innerHTMl change
        for (let ron of document.querySelectorAll('.ron')) {
            ron.addEventListener('click', (event) => {
                event.stopPropagation()
                game.playAudio(game.audio.ron)
                this.players.map(el => {
                    if (el.isRiichi == 1) {
                        el.isFuro = 0;
                        document.querySelector(`.${el.position} .box`).classList.add("blue");
                        document.querySelector(`.${el.position} .sub.menu`).classList.add("blue");
                    };
                });
                const qsRon = event.target.parentElement.parentElement.parentElement.className
                game.fu = [0, 30, 30, 30, 40, 50, 60, 70, 80, 90, 100, 110]
                document.querySelector(`.${CSS.escape(qsRon)} .han`).value = 1
                document.querySelector(`.${CSS.escape(qsRon)} .fu`).value = 3
                document.querySelector(`.${CSS.escape(qsRon)} .han_value`).innerHTML = 1
                document.querySelector(`.${CSS.escape(qsRon)} .fu_value`).innerHTML = 30
                document.querySelector(`.${CSS.escape(qsRon)} .win`).innerHTML = "榮和"
                const previewPoint = game.scorer().ron(game.han[document.querySelector(`.${CSS.escape(qsRon)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsRon)} .fu`).value], game[qsRon].isDealer);
                this[qsRon].previewPoint = previewPoint;
                document.querySelector(`.${CSS.escape(qsRon)} .preview.point`).innerHTML = previewPoint;
                document.querySelector(`.${CSS.escape(qsRon)} .dealer-text`).innerHTML = game[qsRon].isDealer ? "親" : "子"
                document.querySelector(`.${CSS.escape(qsRon)} .repeater`).innerHTML = game.repeater
                document.querySelector(`.${CSS.escape(qsRon)} .sub.menu`).classList.add("activated")
                document.querySelector(`.${CSS.escape(qsRon)} .discarder`).classList.add("activated")
                document.querySelector(`.${CSS.escape(qsRon)} .main.menu`).classList.remove("activated")
            })
        }

        let hanChanged = true;
        const changeHan = (event) => {
            event.stopPropagation();
            let hanValue = event.target.value;
            const qsHan = event.target.parentElement.parentElement.parentElement.className;
            let win_state = document.querySelector(`.${CSS.escape(qsHan)} .win`).innerHTML;
            if (event.target.className === 'han-text') {
                document.querySelector(`.${CSS.escape(qsHan)} .han`).value = Number(document.querySelector(`.${CSS.escape(qsHan)} .han`).value) + (hanChanged ? 1 : -1);
                if (document.querySelector(`.${CSS.escape(qsHan)} .han`).value == 1) hanChanged = true;
                if (document.querySelector(`.${CSS.escape(qsHan)} .han`).value == 9) hanChanged = false;
                hanValue = document.querySelector(`.${CSS.escape(qsHan)} .han`).value;
            };
            game.fu = [0, 30, 30, 30, 40, 50, 60, 70, 80, 90, 100, 110];
            if (hanValue == 9) {
                game.fu = [0, "役滿", "役滿", "役滿", "役滿", "役滿", "役滿", "兩倍役滿", "兩倍役滿", "三倍役滿", "三倍役滿", "四倍役滿"];
            } else if (hanValue == 8) {
                game.fu = [0, "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿", "三倍滿"];
            } else if (hanValue == 7) {
                game.fu = [0, "倍滿", "倍滿", "倍滿", "倍滿", "倍滿", "倍滿", "倍滿", "倍滿", "倍滿", "倍滿", "倍滿"];
            } else if (hanValue == 6) {
                game.fu = [0, "跳滿", "跳滿", "跳滿", "跳滿", "跳滿", "跳滿", "跳滿", "跳滿", "跳滿", "跳滿", "跳滿"];
            } else if (hanValue == 5) {
                game.fu = [0, "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫"];
            } else if (hanValue == 4) {
                game.fu = [0, 25, 25, 30, "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫", "滿貫"];
                if (win_state == "自摸") {
                    game.fu[1] = 20;
                };
            } else if (hanValue == 3) {
                game.fu = [0, 25, 25, 30, 40, 50, 60, "滿貫", "滿貫", "滿貫", "滿貫", "滿貫"];
                if (win_state == "自摸") {
                    game.fu[1] = 20;
                };
            } else if (hanValue == 2) {
                if (win_state == "自摸") {
                    game.fu[1] = 20;
                    game.fu[2] = 20;
                };
                if (win_state == "榮和") {
                    game.fu[1] = 25
                    game.fu[2] = 25
                };
            } else if (hanValue == 1) {
                if (win_state == "自摸") {
                    game.fu[11] = 100;
                };
            } else {
                alert('unhandled case');
            };
            document.querySelector(`.${CSS.escape(qsHan)} .han_value`).innerHTML = game.han[document.querySelector(`.${CSS.escape(qsHan)} .han`).value];
            document.querySelector(`.${CSS.escape(qsHan)} .fu_value`).innerHTML = game.fu[document.querySelector(`.${CSS.escape(qsHan)} .fu`).value];
            if (win_state == "榮和") {
                let previewPoint = game.scorer().ron(game.han[document.querySelector(`.${CSS.escape(qsHan)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsHan)} .fu`).value], game[qsHan].isDealer);
                this[qsHan].previewPoint = previewPoint;
                document.querySelector(`.${CSS.escape(qsHan)} .preview.point`).innerHTML = previewPoint;
            } else if (win_state == "自摸") {
                let previewPoint = game.scorer().tsumo(game.han[document.querySelector(`.${CSS.escape(qsHan)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsHan)} .fu`).value], game[qsHan].isDealer);
                this[qsHan].previewPoint = previewPoint;
                document.querySelector(`.${CSS.escape(qsHan)} .preview.point`).innerHTML = previewPoint;
            };
        };

        // When han input, update the current slider value, calculate and change innerHTMl
        for (let han of document.querySelectorAll('.han')) {
            han.addEventListener('input', changeHan);
        };
        // When click han-text input, update the current slider value, calculate and change innerHTMl
        for (let han of document.querySelectorAll('.han-text')) {
            han.addEventListener('click', changeHan);
        }

        let fuChanged = true;

        const changeFu = (event) => {
            event.stopPropagation();
            const qsFu = event.target.parentElement.parentElement.parentElement.className;
            if (event.target.className === 'fu-text') {
                document.querySelector(`.${CSS.escape(qsFu)} .fu`).value = Number(document.querySelector(`.${CSS.escape(qsFu)} .fu`).value) + (fuChanged ? 1 : -1);
                if (document.querySelector(`.${CSS.escape(qsFu)} .fu`).value == 1) fuChanged = true;
                if (document.querySelector(`.${CSS.escape(qsFu)} .fu`).value == 11) fuChanged = false;
            };
            document.querySelector(`.${CSS.escape(qsFu)} .fu_value`).innerHTML = game.fu[document.querySelector(`.${CSS.escape(qsFu)} .fu`).value];
            if (document.querySelector(`.${CSS.escape(qsFu)} .win`).innerHTML == "榮和") {
                let previewPoint = game.scorer().ron(game.han[document.querySelector(`.${CSS.escape(qsFu)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsFu)} .fu`).value], game[qsFu].isDealer);
                this[qsFu].previewPoint = previewPoint;
                document.querySelector(`.${CSS.escape(qsFu)} .preview.point`).innerHTML = previewPoint;
            } else if (document.querySelector(`.${CSS.escape(qsFu)} .win`).innerHTML == "自摸") {
                let previewPoint = game.scorer().tsumo(game.han[document.querySelector(`.${CSS.escape(qsFu)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsFu)} .fu`).value], game[qsFu].isDealer);
                this[qsFu].previewPoint = previewPoint;
                document.querySelector(`.${CSS.escape(qsFu)} .preview.point`).innerHTML = previewPoint;
            };
        };

        //When fu input, update the current slider value, calculate and change innerHTMl
        for (let fu of document.querySelectorAll('.fu')) {
            fu.addEventListener('input', changeFu);
        }
        //When click top fu-text input, update the current slider value, calculate and change innerHTMl
        for (let fu of document.querySelectorAll('.fu-text')) {
            fu.addEventListener('click', changeFu);
        }
        //When each discarder clicked, change the discarder
        for (let button of document.querySelectorAll('.discarder button')) {
            button.addEventListener('click', (event) => {
                event.stopPropagation()
                document.querySelectorAll('.selected').forEach(el => el.classList.remove("selected"))
                event.target.classList.add("selected")
                const qsButton = event.target.parentElement.parentElement.parentElement.parentElement.className
                game[qsButton].discarder = event.target.className
            })
        }

        //When submenu confirm clicked remove all menu include submenu
        for (let confirm of document.querySelectorAll('.sub.menu .confirm')) {
            confirm.addEventListener('click', async (event) => {
                event.stopPropagation();
                setTimeout(async () => {
                    const isHandlingMultiRon = this.isHandlingMultiRon;
                    const qsConfirm = event.target.parentElement.parentElement.parentElement.parentElement.className;
                    const qsDiscarder = qsConfirm + " " + game[qsConfirm].discarder;
                    const winState = document.querySelector(`.${CSS.escape(qsConfirm)} .win`).innerHTML;
                    let discarder;
                    if (winState == "榮和" && game[qsConfirm].discarder !== "upper seat selected" && game[qsConfirm].discarder !== "opposite seat selected" && game[qsConfirm].discarder !== "lower seat selected") {
                        return;
                    };
                    if (winState == "榮和") {
                        game[qsConfirm].changed += game[qsConfirm].previewPoint;
                        game[qsConfirm].changed += game.deposit.result();
                        switch (qsDiscarder) {
                            case "left upper seat selected":
                            case "bottom opposite seat selected":
                            case "right lower seat selected":
                                game.top.changed -= game[qsConfirm].previewPoint;
                                game.top.add(-game[qsConfirm].previewPoint)
                                discarder = "top"
                                break;
                            case "right upper seat selected":
                            case "top opposite seat selected":
                            case "left lower seat selected":
                                game.bottom.changed -= game[qsConfirm].previewPoint;
                                game.bottom.add(-game[qsConfirm].previewPoint)
                                discarder = "bottom"
                                break;
                            case "bottom upper seat selected":
                            case "right opposite seat selected":
                            case "top lower seat selected":
                                game.left.changed -= game[qsConfirm].previewPoint;
                                game.left.add(-game[qsConfirm].previewPoint)
                                discarder = "left"
                                break;
                            case "top upper seat selected":
                            case "left opposite seat selected":
                            case "bottom lower seat selected":
                                game.right.changed -= game[qsConfirm].previewPoint;
                                game.right.add(-game[qsConfirm].previewPoint)
                                discarder = "right"
                                break;
                        }
                        game[qsConfirm].add(game[qsConfirm].changed);
                        document.querySelector(".top .current.point").innerHTML = game.top.result()
                        document.querySelector(".bottom .current.point").innerHTML = game.bottom.result()
                        document.querySelector(".left .current.point").innerHTML = game.left.result()
                        document.querySelector(".right .current.point").innerHTML = game.right.result()
                        if (!isHandlingMultiRon) {
                            game.changedPointColor();
                            game.players.filter(players => players.isRiichi == 1).map(players => players.changed -= 1000)
                        }
                        game.deposit.set(0);
                        game.multiRonData.push({
                            ron: qsConfirm,
                            han_fu: typeof game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value] == "string" ? game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value] : `${game.han[document.querySelector(`.${CSS.escape(qsConfirm)} .han`).value]}_${game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value]}`,
                            point: game[qsConfirm].previewPoint
                        })
                        if (!isHandlingMultiRon) {
                            if (game.multiRonData.length === 2) {
                                game.record(
                                    {
                                        state: "和了",
                                        han_fu: [game.multiRonData[0].han_fu, game.multiRonData[1].han_fu],
                                        point: [game.multiRonData[0].point, game.multiRonData[1].point],
                                    }
                                );
                            } else {
                                game.record(
                                    {
                                        state: "和了",
                                        han_fu: typeof game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value] == "string" ? game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value] : `${game.han[document.querySelector(`.${CSS.escape(qsConfirm)} .han`).value]}_${game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value]}`,
                                        point: game[qsConfirm].previewPoint
                                    }
                                );
                            }
                        }
                    } else if (winState == "自摸") {
                        if (game.dealer == qsConfirm && game[qsConfirm].previewPoint.split("/").length === 1) {
                            let dealerTsumo = game[qsConfirm].previewPoint.replace(/[^0-9]/g, "");
                            var previewPoint = dealerTsumo * 3;
                            game[qsConfirm].changed += dealerTsumo * 3;
                            game.players_sub = game.players.filter(players => players.position != game.dealer)
                            game.players_sub.map(el => {
                                el.changed -= dealerTsumo
                                el.add(el.changed)
                            })
                        } else {
                            let nonDealerTsumoDouble = parseInt(game[qsConfirm].previewPoint.split("/")[0]);
                            let nonDealerTsumoSingle = parseInt(game[qsConfirm].previewPoint.split("/")[1]);
                            var previewPoint = nonDealerTsumoDouble + nonDealerTsumoSingle * 2;
                            game[qsConfirm].changed += nonDealerTsumoDouble + nonDealerTsumoSingle * 2;
                            game.players_sub = game.players.filter(players => players.position != game.dealer && players.position != qsConfirm);
                            game.players_sub.map(players => players.changed -= nonDealerTsumoSingle);
                            game.players_sub.map(players => players.add(players.changed));
                            game.players.filter(players => players.isDealer == 1).map(players => players.changed -= nonDealerTsumoDouble);
                            game.players.filter(players => players.isDealer == 1).map(players => players.add(players.changed));
                        };
                        game[qsConfirm].changed += game.deposit.result();
                        game[qsConfirm].add(game[qsConfirm].changed);
                        game.changedPointColor();
                        game.players.filter(players => players.isRiichi == 1).map(players => players.changed -= 1000);
                        document.querySelector(`.top .current.point`).innerHTML = game.top.result();
                        document.querySelector(`.bottom .current.point`).innerHTML = game.bottom.result();
                        document.querySelector(`.left .current.point`).innerHTML = game.left.result();
                        document.querySelector(`.right .current.point`).innerHTML = game.right.result();
                        game.deposit.set(0);
                        game.record(
                            {
                                state: "自摸",
                                han_fu: typeof game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value] == "string" ? game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value] : `${game.han[document.querySelector(`.${CSS.escape(qsConfirm)} .han`).value]}_${game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value]}`,
                                point: previewPoint
                            }
                        )
                    } else {
                        alert('unhandled case')
                    }
                    if (!isHandlingMultiRon) {
                        game.resetChanged()
                        game.resetRiichi()
                        game.resetFuro()
                    }
                    document.querySelector(".deposit").innerHTML = 0
                    document.querySelector(`.${CSS.escape(qsConfirm)} .han`).value = 1
                    document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value = 3
                    if (!isHandlingMultiRon) {
                        console.table({
                            game: game.history[game.history.length - 1].gameDetail.round + game.history[game.history.length - 1].gameDetail.hand + "局 " + game.history[game.history.length - 1].gameDetail.repeater + "本場 " + "親: " + game.history[game.history.length - 1].gameDetail.dealer,
                            state: game.history[game.history.length - 1].handDetail.state,
                            han_fu: [].concat(game.history[game.history.length - 1].handDetail.han_fu).join(','),
                            point: [].concat(game.history[game.history.length - 1].handDetail.point).join(','),
                            date: game.history[game.history.length - 1].date
                        })
                        console.table([...game.history[game.history.length - 1].playerDetail, { position: "center", point: game.deposit.point }])
                    }
                    document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"))
                    if (!isHandlingMultiRon) {
                        if (game.multiRonData.length > 0) {
                            game.changeDealer(game.multiRonData.map(x => x.ron));
                        } else {
                            game.changeDealer([qsConfirm]);
                        }
                        game.multiRonData = [];
                    }
                    await game.playHanSound(game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value])
                    if (!isHandlingMultiRon) {
                        game.checkEnd()
                        if (game.isEnd !== 1) this.playGameSound();
                    }
                    if (winState == "榮和") {
                        document.querySelector(`.${CSS.escape(qsConfirm)} .preview.point`).innerHTML = game.scorer().ron(game.han[document.querySelector(`.${CSS.escape(qsConfirm)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value], game[qsConfirm].isDealer)
                    } else if (winState == "自摸") {
                        document.querySelector(`.${CSS.escape(qsConfirm)} .preview.point`).innerHTML = game.scorer().tsumo(game.han[document.querySelector(`.${CSS.escape(qsConfirm)} .han`).value], game.fu[document.querySelector(`.${CSS.escape(qsConfirm)} .fu`).value], game[qsConfirm].isDealer)
                    }
                }, 100);
            })
        }
        //When submenu cancel clicked remove submenu only
        for (let subMenuCancel of document.querySelectorAll('.sub.menu .cancel')) {
            subMenuCancel.addEventListener('click', (event) => {
                if (this.isHandlingMultiRon) return window.alert('冇取消呀，撳確認啦');
                event.stopPropagation();
                const qsSubMenuCancel = event.target.parentElement.parentElement.parentElement.parentElement.className
                document.querySelector(`.${CSS.escape(qsSubMenuCancel)} .sub.menu`).classList.remove("activated")
                game.resetFuro();
            })
        }
        //When submenu cancel clicked remove submenu only
        for (let change of document.querySelectorAll('.main.menu .change')) {
            change.addEventListener('click', (event) => {
                event.stopPropagation()
                const qsChange = event.target.parentElement.parentElement.parentElement.className
                let newPoint = game[qsChange].point
                do {
                    newPoint = prompt("change score?", game[qsChange].point)
                    newPoint = newPoint == null ? game[qsChange].point : parseInt(newPoint)
                } while (newPoint % 100 != 0);
                if (game[qsChange].point != newPoint) {
                    game.changedPointHistory.push(
                        {
                            gameDetail: {
                                round: game.round,
                                hand: game.hand % 4 == 0 ? 4 : game.hand % 4,
                                repeater: game.repeater,
                            },
                            changedDetail: {
                                name: game[qsChange].name,
                                position: game[qsChange].position,
                                point: game[qsChange].point,
                                new_point: newPoint
                            }
                        }
                    )
                    game[qsChange].changed = newPoint - game[qsChange].point
                    game[qsChange].add(game[qsChange].changed)
                    game.changedPointColor()
                    game.resetChanged()
                    document.querySelector(`.${CSS.escape(qsChange)} .current.point`).innerHTML = game[qsChange].point
                }
                document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"))
            })
        }
        //When menu cancel-riichi clicked, current point innerHTMl change and player result +1000, riichi innerHTML from 取消立直 change to 立直
        for (let riichi of document.querySelectorAll('.riichi')) {
            riichi.addEventListener('click', (event) => {
                event.stopPropagation()
                const qsRiichi = event.target.parentElement.parentElement.parentElement.className
                if (document.querySelector(`.${CSS.escape(qsRiichi)} .stick`).classList.contains("activated")) {
                    document.querySelector(`.${CSS.escape(qsRiichi)} .stick`).classList.remove("activated")
                    game[qsRiichi].add(1000)
                    game[qsRiichi].isRiichi = 0
                    document.querySelector(`.${CSS.escape(qsRiichi)} .current.point`).innerHTML = game[qsRiichi].result()
                    game.deposit.sub(1000)
                    document.querySelector(".deposit").innerHTML = game.deposit.point / 1000
                    //When menu riichi clicked, current point innerHTMl change and player result -1000, riichi innerHTML from 立直 change to 取消立直
                } else {
                    //may riichi point 1000 or above
                    if (game[qsRiichi].point >= 1000) {
                        game.playAudio(game.audio.reach)
                        document.querySelector(`.${CSS.escape(qsRiichi)} .stick`).classList.add("activated")
                        game[qsRiichi].sub(1000)
                        game[qsRiichi].isRiichi = 1
                        document.querySelector(`.${CSS.escape(qsRiichi)} .current.point`).innerHTML = game[qsRiichi].result()
                        game.deposit.add(1000)
                        document.querySelector(".deposit").innerHTML = game.deposit.point / 1000
                    } else {
                        return
                    }
                }
                document.querySelector(`.${CSS.escape(qsRiichi)} .main.menu`).classList.remove("activated")
            })
        }
    }
    async playGameSound() {
        const roundSound = {
            "東": 'ton',
            "南": 'nan',
            "西": 'sha',
            "北": 'pei'
        }
        const handSound = [null, 'ikkyoku', 'nikyoku', 'sankyoku', 'yonkyoku']
        const repeaterSound = [null, 'ipponba', 'nihonba', 'sanbonba', 'yonhonba', 'gohonba', 'ropponba', 'nanahonba', 'happonba', 'kyuhonba', 'jupponba', 'juipponba', 'junihonba', 'jusanbonba', 'juyonhonba', 'jugohonba', 'juropponba']
        await this.playAudio(this.audio[roundSound[this.round]])
        await this.playAudio(this.audio[handSound[this.hand % 4 == 0 ? 4 : this.hand % 4]])
        if (this.repeater >= 1 && this.repeater <= 16) await this.playAudio(this.audio[repeaterSound[this.repeater]])
    }

    async playHanSound(fuString) {
        if (fuString == "役滿" || fuString == "兩倍役滿" || fuString == "三倍役滿" || fuString == "四倍役滿") {
            await game.playAudio(game.audio.yakuman)
        } else if (fuString == "三倍滿") {
            await game.playAudio(game.audio.sanbaiman)
        } else if (fuString == "倍滿") {
            await game.playAudio(game.audio.baiman)
        } else if (fuString == "跳滿") {
            await game.playAudio(game.audio.haneman)
        } else if (fuString == "滿貫") {
            await game.playAudio(game.audio.mangan)
        } else {
            await game.playAudio(game.audio.agari)
        }
    }

    checkEnd() {
        let ranking = this.players.slice().sort(function (a, b) {
            return a.point > b.point ? -1 : a.point < b.point ? 1 : a.tiePriority > b.tiePriority ? -1 : 1;
        })
        if (this.players.filter(players => players.point < 0).length >= 1) {
            // any player point below zero
            this.end()
        } else if (this.hand >= 9 && this.players.filter(players => players.point >= 30000).length >= 1) {
            this.end()
        } else if (this.hand === 8 && this.repeater >=1 && ranking[0].isDealer === 1 && ranking[0].point >= 30000) {
            let confirm = window.confirm('end?')
            if(confirm) {
                this.end()
            }
        } else if (this.hand >= 13) {
            this.end()
        }
    }

    //calculate end score
    async end() {
        if (game.isEnd == 0) {
            let ranking = game.players.slice().sort(function (a, b) {
                return a.point > b.point ? -1 : a.point < b.point ? 1 : a.tiePriority > b.tiePriority ? -1 : 1;
            })
            ranking[0].rankingScore = 40
            ranking[1].rankingScore = 10
            ranking[2].rankingScore = -10
            ranking[3].rankingScore = -20
            //rank 1 player get deposit
            ranking[0].point = ranking[0].point + game.deposit.point
            game.deposit.set(0)

            let finalScore = (point) => (point - this.originPoint) / 1000
            let playerDetail = [
                { name: ranking[0].name, position: ranking[0].position, rank: 1, point: ranking[0].point, score: finalScore(ranking[0].point), ranking_score: ranking[0].rankingScore, total_score: Math.round((finalScore(ranking[0].point) + ranking[0].rankingScore) * 10) / 10 },
                { name: ranking[1].name, position: ranking[1].position, rank: 2, point: ranking[1].point, score: finalScore(ranking[1].point), ranking_score: ranking[1].rankingScore, total_score: Math.round((finalScore(ranking[1].point) + ranking[1].rankingScore) * 10) / 10 },
                { name: ranking[2].name, position: ranking[2].position, rank: 3, point: ranking[2].point, score: finalScore(ranking[2].point), ranking_score: ranking[2].rankingScore, total_score: Math.round((finalScore(ranking[2].point) + ranking[2].rankingScore) * 10) / 10 },
                { name: ranking[3].name, position: ranking[3].position, rank: 4, point: ranking[3].point, score: finalScore(ranking[3].point), ranking_score: ranking[3].rankingScore, total_score: Math.round((finalScore(ranking[3].point) + ranking[3].rankingScore) * 10) / 10 }
            ];
            playerDetail.sort((a, b) => {
                let sorting = ['top', 'left', 'bottom', 'right'];
                return sorting.indexOf(a.position) < sorting.indexOf(b.position) ? -1 : 1;
            })
            this.history.push(
                {
                    date: new Date().toLocaleString('en-US'),
                    playerDetail: playerDetail
                }
            )
            console.table(game.history[game.history.length - 1].playerDetail)
            game.isEnd = 1
            let id = await this.send()
            document.getElementsByClassName('result')[0].style.display = 'block'
            document.querySelector('.result').addEventListener('click', () => {
                if (navigator.share) {
                    navigator.share({
                        title: '對局紀錄',
                        url: `${window.location.origin}/result/index.html?id=${id}`
                    })
                        .then(() => console.log('Successful share'))
                        .catch((error) => console.log('Error sharing', error));
                    window.open(`${window.location.origin}/result/index.html?id=${id}`)
                } else {
                    window.open(`${window.location.origin}/result/index.html?id=${id}`)
                }
            })
            await game.playAudio(game.audio.end)
        }
    }

    newGame() {
        const initial_point = 25000
        this.top = new Player("top", initial_point)
        this.bottom = new Player("bottom", initial_point)
        this.left = new Player("left", initial_point)
        this.right = new Player("right", initial_point)
        this.deposit = new Deposit("center", 0)
        this.hand = 1 //e1,e2,e3,e4,e4-1,s1 =1,2,3,4,4,5
        this.repeater = 0
        this.draw = 0
        this.isEnd = 0
        this.nonCallPoint = 3000
        this.round = "東"
        this.initialPoint = initial_point
        this.originPoint = initial_point + 5000
        this.history = [] //this.history.length - 1 = total hand(s) played
        this.isConfirm = 0
        this.isHandlingMultiRon = false
        this.players = [this.top, this.left, this.bottom, this.right]
        this.players_add = []
        this.players_sub = []
        this.changedPointHistory = []
        this.multiRonData = [];
        this.uuid = ""

        document.querySelector(".hand").innerHTML = game.hand % 4 == 0 ? 4 : game.hand % 4
        document.querySelector(".roundText").innerHTML = game.round
        document.querySelector(".repeater").innerHTML = game.repeater
        document.querySelector(".deposit").innerHTML = game.deposit.point / 1000
        document.querySelector(`.box.dealer`).classList.remove('dealer')
        document.querySelectorAll(` .current.point`).forEach(el => el.innerHTML = 25000)
        //remove result listener
        let el = document.getElementsByClassName('result')[0];
        let elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);

        document.getElementsByClassName('result')[0].style.display = 'none'
        document.querySelectorAll('.menu').forEach(el => el.classList.remove("activated"))
        document.querySelectorAll('.stick').forEach(el => el.classList.remove("activated"))

        addDealerEventListerner('restart')
    }

    resetDiscarder() {
        this.top.discarder = 0
        this.bottom.discarder = 0
        this.left.discarder = 0
        this.right.discarder = 0
    }
    resetCalling() {
        this.top.isCalling = 0
        this.bottom.isCalling = 0
        this.left.isCalling = 0
        this.right.isCalling = 0
    }
    resetFuro() {
        this.top.isFuro = undefined
        this.bottom.isFuro = undefined
        this.left.isFuro = undefined
        this.right.isFuro = undefined
        document.querySelectorAll('.box').forEach(el => { el.classList.remove("red"); el.classList.remove("blue") })
        document.querySelectorAll('.sub.menu').forEach(el => { el.classList.remove("red"); el.classList.remove("blue") })
    }
    resetRiichi() {
        this.top.isRiichi = 0
        this.bottom.isRiichi = 0
        this.left.isRiichi = 0
        this.right.isRiichi = 0
        document.querySelectorAll('.riichi').forEach(el => el.innerHTML = "立直")
        document.querySelectorAll('.stick').forEach(el => el.classList.remove("activated"))
    }
    resetChanged() {
        this.top.changed = 0
        this.bottom.changed = 0
        this.left.changed = 0
        this.right.changed = 0
    }
    showPointDifference(boolean) {
        if (boolean) {
            this.players.map(el => {
                document.querySelector(`.${CSS.escape(el.position)} .player-name`).style.display = 'none';
                if (document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML >= 0
                    || document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML === '-') {
                    if (document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML > 0) document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML = "+" + document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML;
                    document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.add("positive");
                } else if (document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML < 0) {
                    document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.add("negative");
                };
            });
        } else {
            this.players.map(el => {
                document.querySelector(`.${CSS.escape(el.position)} .player-name`).style.display = 'block';
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.remove("positive");
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.remove("negative");
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.remove("show")
            });
        };
    };
    changedPointColor() {
        this.players.slice().map(el => {
            document.querySelector(`.${CSS.escape(el.position)} .player-name`).style.display = 'none';
            if (el.changed > 0) {
                el.changed = "+" + el.changed
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML = el.changed
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.add("positive")
            } else if (el.changed < 0) {
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).innerHTML = el.changed
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.add("negative")
            } else {
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.add("show")
            }
        })
        setTimeout(() => {
            this.players.map(el => {
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.remove("positive")
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.remove("negative")
                document.querySelector(`.${CSS.escape(el.position)} .changed.point`).classList.remove("show")
                document.querySelector(`.${CSS.escape(el.position)} .player-name`).style.display = 'block';
            })
        }, 3000);
        this.players.map(el => el.changed = parseInt(el.changed))
    }
    changeDealer(winner) {
        //dealer win, repeater+1
        if (Array.isArray(winner) && winner.find(x => x === this.dealer)) {
            this.repeater += 1
        } else {
            //no winner, it is draw
            if (winner.length === 0) {
                this.repeater += 1
                //non-dealer win, repeater set to 0
            } else {
                this.repeater = 0
            }
            //either draw or non-dealer win, hand +1, change each round 1st 局 by 東南西北 order and dealer by anti-clockwise
            this.hand += 1
            if (this.hand % 4 == 1) {
                switch (this.round) {
                    case "東":
                        this.round = "南"
                        break;
                    case "南":
                        this.round = "西"
                        break;
                    case "西":
                        this.round = "北"
                        break;
                    case "北":
                        this.round = "東"
                        break;
                    default:
                        this.round = ""
                        break;
                }
            }

            document.querySelectorAll(".box").forEach(el => el.classList.remove("dealer"))
            switch (this.dealer) {
                case "top":
                    game.top.isDealer = 0
                    game.left.isDealer = 1
                    this.dealer = "left"
                    document.querySelector(".left .box").classList.add("dealer")
                    break;
                case "left":
                    game.left.isDealer = 0
                    game.bottom.isDealer = 1
                    this.dealer = "bottom"
                    document.querySelector(".bottom .box").classList.add("dealer")
                    break;
                case "bottom":
                    game.bottom.isDealer = 0
                    game.right.isDealer = 1
                    this.dealer = "right"
                    document.querySelector(".right .box").classList.add("dealer")
                    break;
                case "right":
                    game.right.isDealer = 0
                    game.top.isDealer = 1
                    this.dealer = "top"
                    document.querySelector(".top .box").classList.add("dealer")
                    break;
                default:
                    this.dealer = ""
                    break;
            }
        }
        document.querySelector(".hand").innerHTML = this.hand % 4 == 0 ? 4 : this.hand % 4
        document.querySelector(".roundText").innerHTML = this.round
        document.querySelector(".repeater").innerHTML = this.repeater
    }
    scorer() {
        return {
            tsumo: (han, fu, isDealer_variable) => {
                const dealerMulti = isDealer_variable == 1 ? 6 : 4
                const repeaterPoint = this.repeater * 100
                let limitMulti = 0
                switch (han) {
                    case 13:
                        switch (fu) {
                            case "兩倍役滿":
                                limitMulti = 1
                                break;
                            case "三倍役滿":
                                limitMulti = 2
                                break;
                            case "四倍役滿":
                                limitMulti = 3
                                break;
                            default:
                                limitMulti = 0
                                break;
                        }
                        if (dealerMulti == 4) {
                            return (dealerMulti / 2 * this.basicPoint[4 + limitMulti] + repeaterPoint) + "/" + (dealerMulti / 4 * this.basicPoint[4 + limitMulti] + repeaterPoint)
                        } else {
                            return "∀" + (dealerMulti / 3 * this.basicPoint[4 + limitMulti] + repeaterPoint)
                        }
                    case "11-12":
                        if (dealerMulti == 4) {
                            return (dealerMulti / 2 * this.basicPoint[3] + repeaterPoint) + "/" + (dealerMulti / 4 * this.basicPoint[3] + repeaterPoint)
                        } else {
                            return "∀" + (dealerMulti / 3 * this.basicPoint[3] + repeaterPoint)
                        }
                    case "8-10":
                        if (dealerMulti == 4) {
                            return (dealerMulti / 2 * this.basicPoint[2] + repeaterPoint) + "/" + (dealerMulti / 4 * this.basicPoint[2] + repeaterPoint)
                        } else {
                            return "∀" + (dealerMulti / 3 * this.basicPoint[2] + repeaterPoint)
                        }
                    case "6-7":
                        if (dealerMulti == 4) {
                            return (dealerMulti / 2 * this.basicPoint[1] + repeaterPoint) + "/" + (dealerMulti / 4 * this.basicPoint[1] + repeaterPoint)
                        } else {
                            return "∀" + (dealerMulti / 3 * this.basicPoint[1] + repeaterPoint)
                        }
                    case 5:
                        if (dealerMulti == 4) {
                            return (dealerMulti / 2 * this.basicPoint[0] + repeaterPoint) + "/" + (dealerMulti / 4 * this.basicPoint[0] + repeaterPoint)
                        } else {
                            return "∀" + (dealerMulti / 3 * this.basicPoint[0] + repeaterPoint)
                        }
                    case 4:
                    case 3:
                    case 2:
                    case 1:
                        if (dealerMulti == 4) {
                            if (fu == "滿貫") {
                                return (dealerMulti / 2 * this.basicPoint[0] + repeaterPoint) + "/" + (dealerMulti / 4 * this.basicPoint[0] + repeaterPoint)
                            } else {
                                return Math.min(
                                    (dealerMulti / 2 * this.basicPoint[0] + repeaterPoint),
                                    (Math.ceil(dealerMulti / 2 * fu * Math.pow(2, (2 + han)) / 100) * 100 + repeaterPoint)
                                ) + "/" + Math.min(
                                    (dealerMulti / 4 * this.basicPoint[0] + repeaterPoint),
                                    (Math.ceil(dealerMulti / 4 * fu * Math.pow(2, (2 + han)) / 100) * 100 + repeaterPoint)
                                )

                            }

                        } else {
                            if (fu == "滿貫") {
                                return "∀" + (dealerMulti / 3 * this.basicPoint[0] + repeaterPoint)
                            } else {
                                return "∀" + Math.min(
                                    (dealerMulti / 3 * this.basicPoint[0] + repeaterPoint),
                                    (Math.ceil(dealerMulti / 3 * fu * Math.pow(2, (2 + han)) / 100) * 100 + repeaterPoint)
                                )
                            }

                        }
                    default:
                        return 0
                }
            },
            ron: (han, fu, isDealer_variable) => {
                const dealerMulti = 4 + 2 * isDealer_variable
                const repeaterPoint = this.repeater * 100
                let limitMulti = 0
                switch (han) {
                    case 13:
                        switch (fu) {
                            case "兩倍役滿":
                                limitMulti = 1
                                break;
                            case "三倍役滿":
                                limitMulti = 2
                                break;
                            case "四倍役滿":
                                limitMulti = 3
                                break;
                            default:
                                limitMulti = 0
                                break;
                        }
                        return dealerMulti * this.basicPoint[4 + limitMulti] + repeaterPoint * 3
                    case "11-12":
                        return dealerMulti * this.basicPoint[3] + repeaterPoint * 3
                    case "8-10":
                        return dealerMulti * this.basicPoint[2] + repeaterPoint * 3
                    case "6-7":
                        return dealerMulti * this.basicPoint[1] + repeaterPoint * 3
                    case 5:
                        return dealerMulti * this.basicPoint[0] + repeaterPoint * 3
                    case 4:
                    case 3:
                    case 2:
                    case 1:
                        if (fu == "滿貫") {
                            return dealerMulti * this.basicPoint[0] + repeaterPoint * 3
                        } else {
                            return Math.min(
                                dealerMulti * this.basicPoint[0] + repeaterPoint * 3,
                                (Math.ceil(dealerMulti * fu * Math.pow(2, (2 + han)) / 100) * 100 + repeaterPoint * 3)
                            )
                        }
                    default:
                        return 0
                }
            }
        }
    }
}

addDealerEventListerner();
const game = new Game(25000);
userGestures();
