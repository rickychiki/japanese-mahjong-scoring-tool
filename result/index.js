async function getJson() {
    let url = new URL(window.location.href);
    let ids = url.searchParams.get("id").split(',');
    let idsMap = ids.map(async (id, i) => {
        try {
            const res = await fetch(`/result/${id}.json`, {
                method: "GET"
            });
            let data = await res.json();
            console.log(data);
            let table = document.createElement('table');
            table.setAttribute('id', i);
            let thead = document.createElement('thead');
            let tbody = document.createElement('tbody');
            let tfoot = document.createElement('tfoot');
            table.append(thead);
            table.append(tbody);
            table.append(tfoot);
            document.getElementsByTagName('body')[0].append(table);
            data.map((item, index) => {
                if (index === 0) {
                    let start = item.starting_player;
                    thead.innerHTML = `
                    <tr>
                    <th>${i + 1}回戰</th>
                    <th>${start === 'top' ? '東' : start === 'left' ? '北' : start === 'bottom' ? '西' : '南'}</th>
                    <th>${start === 'top' ? '南' : start === 'left' ? '東' : start === 'bottom' ? '北' : '西'}</th>
                    <th>${start === 'top' ? '西' : start === 'left' ? '南' : start === 'bottom' ? '東' : '北'}</th>
                    <th>${start === 'top' ? '北' : start === 'left' ? '西' : start === 'bottom' ? '南' : '東'}</th>
                    <th>詳情</th>
                    <th>飜和符</th>
                    <th>時間</th>
                    </tr>
                    `
                    tbody.innerHTML = `
                    <tr>
                    <td></td>
                    <td>${item.playerDetail[0].name}</td>
                    <td>${item.playerDetail[1].name}</td>
                    <td>${item.playerDetail[2].name}</td>
                    <td>${item.playerDetail[3].name}</td>
                    <td></td>
                    <td></td>
                    <td>${item.date}</td>
                    </tr>
                    <tr>
                    <td></td>
                    <td>${item.playerDetail[0].point}</td>
                    <td>${item.playerDetail[1].point}</td>
                    <td>${item.playerDetail[2].point}</td>
                    <td>${item.playerDetail[3].point}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                    `;
                } else if (index < data.length - 1) {
                    let hanAndFu = '';
                    if (item.handDetail.han_fu) {
                        let han_fu_array = [].concat(item.handDetail.han_fu);
                        hanAndFu = han_fu_array.map(x => {
                            if (x.includes('_')) {
                                return x.split('_')[0] + '飜' + x.split('_')[1] + '符'
                            } else {
                                return x
                            }
                        }).join(',');
                    } else if (item.handDetail.abortive) {
                        hanAndFu = item.handDetail.abortive
                    } else {
                        hanAndFu = ''
                    }
                    tbody.innerHTML += `
                    <tr>
                    <td>${item.gameDetail.round}${item.gameDetail.hand}局${item.gameDetail.repeater}本場</td>
                    <td ${item.playerDetail[0].changed >= 0 ? 'class=black' : 'class=red'}${item.playerDetail[0].changed === 0? ' style=text-align:left': ''}>${item.playerDetail[0].furo === true? 'x ': item.playerDetail[0].riichi === true? '○ ': ''}${item.playerDetail[0].changed === 0 ? '' : item.playerDetail[0].changed}</td>
                    <td ${item.playerDetail[1].changed >= 0 ? 'class=black' : 'class=red'}${item.playerDetail[1].changed === 0? ' style=text-align:left': ''}>${item.playerDetail[1].furo === true? 'x ': item.playerDetail[1].riichi === true? '○ ': ''}${item.playerDetail[1].changed === 0 ? '' : item.playerDetail[1].changed}</td>
                    <td ${item.playerDetail[2].changed >= 0 ? 'class=black' : 'class=red'}${item.playerDetail[2].changed === 0? ' style=text-align:left': ''}>${item.playerDetail[2].furo === true? 'x ': item.playerDetail[2].riichi === true? '○ ': ''}${item.playerDetail[2].changed === 0 ? '' : item.playerDetail[2].changed}</td>
                    <td ${item.playerDetail[3].changed >= 0 ? 'class=black' : 'class=red'}${item.playerDetail[3].changed === 0? ' style=text-align:left': ''}>${item.playerDetail[3].furo === true? 'x ': item.playerDetail[3].riichi === true? '○ ': ''}${item.playerDetail[3].changed === 0 ? '' : item.playerDetail[3].changed}</td>
                    <td>${item.handDetail.state}</td>
                    <td>${hanAndFu}</td>
                    <td>${item.date}</td>
                    </tr>
                    <tr>
                    <td>供托${item.gameDetail.deposit}</td>
                    <td ${item.gameDetail.dealer === 'top' ? 'class=highlight' : ''}>${item.playerDetail[0].point}</td>
                    <td ${item.gameDetail.dealer === 'left' ? 'class=highlight' : ''}>${item.playerDetail[1].point}</td>
                    <td ${item.gameDetail.dealer === 'bottom' ? 'class=highlight' : ''}>${item.playerDetail[2].point}</td>
                    <td ${item.gameDetail.dealer === 'right' ? 'class=highlight' : ''}>${item.playerDetail[3].point}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                    `;
                } else {
                    tfoot.innerHTML = `
                    <tr>
                    <td>總分</td>
                    <td>${Number(item.playerDetail.find(x => x.position === 'top').total_score).toFixed(1)}</td>
                    <td>${Number(item.playerDetail.find(x => x.position === 'left').total_score).toFixed(1)}</td>
                    <td>${Number(item.playerDetail.find(x => x.position === 'bottom').total_score).toFixed(1)}</td>
                    <td>${Number(item.playerDetail.find(x => x.position === 'right').total_score).toFixed(1)}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                    `;
                };
            });
        } catch (e) {
            document.getElementsByTagName('body')[0].innerHTML = 'result not found';
            console.log(e)
        };
    });
    await Promise.all(idsMap);
    Array.prototype.slice.call(document.getElementsByTagName('table'))
        .sort(function (a, b) { return a.id < b.id ? -1 : 1 })
        .map(x => document.getElementsByTagName('body')[0].appendChild(x));
};

getJson();