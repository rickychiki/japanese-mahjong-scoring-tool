import * as express from 'express'
import * as path from 'path'
import * as bodyParser from 'body-parser'
import * as fs from 'fs'
import * as jsonfile from 'jsonfile';
import {v4 as uuidv4} from 'uuid';
// const JSON_FILE = path.join(__dirname, 'result.json');
const PORT = 8080
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', (req, res, next) => {
    const ip = req.headers['x-real-ip'] || req.connection.remoteAddress
    const date = new Date().toLocaleString('zh').split(', ');
    console.log(`${ip} [${date}] Request ${req.url}`);
    next();
});
app.use(express.static('public'))
app.get('/start', async (req, res) => {
    res.json(uuidv4())
});
app.use('/node_modules', express.static('node_modules'));
app.use('/result', express.static('result'));
app.post('/server/:uuid', async (req, res) => {
    const id = req.params.uuid || uuidv4();
    // const results: string[] = await jsonfile.readFile(JSON_FILE);
    // console.log(req.body)
    const result = req.body;
    // result.sessionID = req.headers.cookie
    // results.push(result);
    const JSON_FILE = path.join(__dirname, 'result', `${id}.json`)
    jsonfile.writeFile(JSON_FILE, result, { spaces: 2 });
    res.json(id)
});
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});
app.use((req, res) => {
    res.status(404).sendFile(path.join(__dirname, "404.html"));
});
app.listen(PORT, () => {
    console.log(`server started on ${PORT}`)
})